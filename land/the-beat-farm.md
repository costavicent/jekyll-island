---
layout: page
title: beat farm
permalink: /land/beat-farm/
---

At the wee hours of the night in the crypts of [WRUW-FM](https://wruw.org), I occasionally farm hip-hop beats, grooves, and jams at a frequency of 91.1 MHz.

Sadly, I've put the beat farm on hold until I finish up school. I hope to return soon!
